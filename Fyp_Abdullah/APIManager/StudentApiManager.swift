//
//  StudentApiManager.swift
//  Fyp_Abdullah
//
//  Created by Abdullah on 11/08/2021.
//

import Foundation
import Alamofire
import SwiftyJSON
struct studentURLS {
    static var LOGIN = APIURLS.StudentURL + "GetStudent"
    static var StudentList = APIURLS.StudentURL + "GetAllStudents"
    static var StudentSemester = APIURLS.BASEURL + "Semester/GetStudentDetails"
}
class StudentAPIManager{
    static func getStudentWith(_ json: [String : Any]) -> Student1  {
        var temp: Student1 = Student1()
        temp.Reg_No = json["Reg_No"] as? String
        temp.App_no = json["Password"] as? String
        temp.Acc_status = json["Acc_status"] as? String
        temp.Ad_status = json["Ad_status"] as? String
        temp.DOMICILE = json["DOMICILE"] as? String
        temp.Deg_No = json["Deg_No"] as? String
        temp.Degree_Received = json["Degree_Received"] as? String
        temp.Enrl_status = json["Enrl_status"] as? String
        temp.Father_name = json["Father_name"] as? String
        temp.Fin_status = json["Fin_status"] as? String
        temp.Final_course = json["Final_course"] as? String
        temp.Marital_status = json["Marital_status"] as? String
        temp.Nic_no = json["Nic_no"] as? String
        temp.PMobile = json["PMobile"] as? String
        temp.Password = json["Password"] as? String
        temp.Per_city = json["Per_city"] as? String
        temp.Pr_city = json["Pr_city"] as? String
        temp.Pref_1 = json["Pref_1"] as? String
        temp.Pref_2 = json["Pref_2"] as? String
        temp.Remarks = json["Remarks"] as? String
        temp.SOS = json["SOS"] as? String
        temp.Section = json["Section"] as? String
        temp.Semester_no = json["Semester_no"] as? String
        temp.Sess = json["Sess"] as? String
        temp.Sex = json["Sex"] as? String
        temp.St_assets = json["St_assets"] as? String
        temp.St_email = json["St_email"] as? String
        temp.St_firstname = json["St_firstname"] as? String
        temp.St_middlename = json["St_middlename"] as? String
        temp.St_lastname = json["St_lastname"] as? String
        temp.St_status = json["St_status"] as? String
        temp.USER_ID = json["USER_ID"] as? String
        temp.Uaar_reg_no = json["Uaar_reg_no"] as? String
        temp.Whom_STATUS = json["Whom_STATUS"] as? String
        temp.Year = json["Year"] as? String
        
        return temp
    }
    
    static func StudentLogin(Reg_No: String, Password: String, completion: @escaping(Student1?) -> ()){
        let Request: [String: Any] = [
            "Reg_No" : Reg_No,
            "Password" : Password
        ]
        print(studentURLS.LOGIN)
        AF.request(studentURLS.LOGIN,
                   method: .post,
                   parameters: Request,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    
                    print("Response:")
                    print(response.result)
                    
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [String: Any] {
                            let studentData = self.getStudentWith(jsonData)
                            completion(studentData)
                        } else {
                            completion(nil)
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion(nil)
                    }
                    
                   }
    }
    static func getStudentSemesterDataWith(_ json: [String : Any]) -> Student  {
        var temp: Student = Student()
        temp.CGPA = json["CGPA"] as? String
        temp.Discipline = json["Discipline"] as? String
        temp.FatherName = json["FatherName"] as? String
        temp.StudentName = json["StudentName"] as? String
        temp.JoinYear = json["JoinYear"] as? String
        temp.Reg_NO = json["Reg_NO"] as? String
        temp.Section = json["Section"] as? String
        temp.Semester_No = json["Semester_No"] as? String
        temp.Semester_Status = json["Semester_Status"] as? String
        temp.TotalCreditHours = json["TotalCreditHours"] as? String
        temp.TotalCreditHoursCleared = json["TotalCreditHoursCleared"] as? String
        return temp
    }
    static func StudentSemester(completion: @escaping(Student) -> ()){
        let Request: [String: Any] = [
            "Reg_No" : Appstatemanager.shared.loggedInUser.Reg_No
        ]
        AF.request(studentURLS.StudentSemester,method: .post, parameters: Request, encoding: JSONEncoding.default).responseJSON{ response in
            
            print("Response:")
            print(response.result)
            
            switch response.result {
            case .success(let value):
                
                if let jsonData = value as? [String: Any] {
                    let studentData = self.getStudentSemesterDataWith(jsonData)
                    completion(studentData)
                } else {
                    completion(Student())
                }
                
            case .failure(let error):
                print("Error:", error)
                completion(Student())
            }
            
           }
    }
}
