//
//  CourseAPIManager.swift
//  Fyp_Abdullah
//
//  Created by Abdullah on 16/08/2021.
//

import Foundation
import Alamofire
import SwiftyJSON
//this is task
struct courseURLS {
    static var CourseList = APIURLS.CourseURL + "GetCourse"
    static var CourseFailed = APIURLS.CourseURL + "GetFailedCourses"
}
class CourseAPIManager {
    static func getCourseWith(_ json: [[String : Any]]) -> [Course1]{
        var AllCoursesData = [Course1]()
        for record in json{
            var temp = Course1()
            temp.Course_no = record["Course_no"] as? String
            temp.ATTEMPT_NO = record["ATTEMPT_NO"] as? String
            temp.REG_NO = record["REG_NO"] as? String
            temp.Emp_no = record["Emp_no"] as? String
            temp.SEMESTER_NO = record["SEMESTER_NO"] as? String
            temp.Course_acw = record["Course_acw"] as? String
            temp.SECTION = record["SECTION"] as? String
            temp.Final_score = record["Final_score"] as? String
            temp.Midterm_score = record["Midterm_score"] as? String
            temp.Assi_score = record["Assi_score"] as? String
            temp.Prac_score = record["Prac_score"] as? String
            temp.Grade = record["Grade"] as? String
            temp.Q_points = record["Q_points"] as? String
            temp.USER_ID = record["USER_ID"] as? String
            temp.DISCIPLINE = record["DISCIPLINE"] as? String
            temp.PRACA = record["PRACA"] as? String
            temp.PRACB = record["PRACB"] as? String
            temp.PRACC = record["PRACC"] as? String
            temp.PRACD = record["PRACD"] as? String
            temp.PRACE = record["PRACE"] as? String
            temp.SOS = record["SOS"] as? String
            temp.pGrade = record["pGrade"] as? String
            temp.Mid_att = record["Mid_att"] as? String
            temp.Fin_att = record["Fin_att"] as? String
            AllCoursesData.append(temp)
        }
        return AllCoursesData
    }
    static func StudentFailedCourses(Reg_No: String, completion: @escaping([Course1]?) -> ()){
        let Request: [String: Any] = [
            "Reg_No" : Reg_No
        ]
        AF.request(courseURLS.CourseFailed,
                   method: .post,
                   parameters: Request,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    
                    print("Response:")
                    print(response.result)
                    
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [[String: Any]] {
                            let CourseData = self.getCourseWith(jsonData)
                            completion(CourseData)
                        } else {
                            completion([Course1]())
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion([Course1]())
                    }
                    
                   }
    }
    static func StudentCourses(Reg_No: String,completion: @escaping([Course1]?) -> ()){
        let Request: [String: Any] = [
            "Reg_No" : Reg_No
        ]
        print(courseURLS.CourseList)
        AF.request(courseURLS.CourseList,
                   method: .post,
                   parameters: Request,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    
                    print("Response:")
                    print(response.result)
                    
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [[String: Any]] {
                            let CourseData = self.getCourseWith(jsonData)
                            completion(CourseData)
                        } else {
                            completion([Course1]())
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion([Course1]())
                    }
                    
                   }
    }
}
