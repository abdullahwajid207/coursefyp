//
//  APIURLS.swift
//  Fyp_Abdullah
//
//  Created by Shh MacBook Pro 2019 on 11/08/2021.
//

import Foundation
class APIURLS{
    static var BASEURL = "http://10.211.55.3:1275/api/"
    static var StudentURL = BASEURL + "Student/"
    static var CourseURL = BASEURL + "Course/"
    static var SemesterURL = BASEURL + "Semester/"
}
