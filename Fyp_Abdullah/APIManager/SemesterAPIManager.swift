//
//  SemesterAPIManager.swift
//  Fyp_Abdullah
//
//  Created by Abdullah on 21/08/2021.
//

import Foundation
import Alamofire
import SwiftyJSON
//this is task
struct SemesterURLS {
    static var SemesterList = APIURLS.SemesterURL + "StudentSemesterDetails"
    static var StudentResult = APIURLS.SemesterURL + "StudentResult"
}
class SemesterAPIManager {
    static func getSemesterResultWith(_ json: [[String : Any]]) -> [Result]{
        var SemesterData = [Result]()
        for record in json{
            var temp = Result()
            temp.Course = record["Course_no"] as? String
            temp.FinalScore = record["Final_score"] as? Double
            temp.Grade = (record["Grade"] as? String)
            temp.Attempt = record["ATTEMPT_NO"] as? String
            temp.PreviousGrade = record["pGrade"] as? String
            SemesterData.append(temp)
        }
        return SemesterData
    }
    static func StudentSemesterResult(Sem_No:String, completion: @escaping([Result]?) -> ()){
        let Request: [String: Any] = [
            "Reg_No" : Appstatemanager.shared.loggedInUser.Reg_No,
            "JoinYear" : Sem_No
        ]
        AF.request(SemesterURLS.StudentResult,
                   method: .post,
                   parameters: Request,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    
                    print("Response:")
                    print(response.result)
                    
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [[String: Any]] {
                            let SemesterData = self.getSemesterResultWith(jsonData)
                            completion(SemesterData)
                        } else {
                            completion([Result]())
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion([Result]())
                    }
                    
                   }
    }
    static func getSemesterWith(_ json: [[String : Any]]) -> [Semesters]{
        var SemesterData = [Semesters]()
        for record in json{
            var temp = Semesters()
            temp.Semester = record["SEMESTER_NO"] as? String
            temp.Status = record["SEM_STATUS"] as? String
            temp.SemesterNum = (record["SemC"] as? Int)
            temp.CGPA = record["CGPA"] as? Double
            SemesterData.append(temp)
        }
        return SemesterData
    }
    static func StudentSemesterData(Reg_No: String, completion: @escaping([Semesters]?) -> ()){
        let Request: [String: Any] = [
            "Reg_No" : Reg_No
        ]
        AF.request(SemesterURLS.SemesterList,
                   method: .post,
                   parameters: Request,
                   encoding: JSONEncoding.default).responseJSON{ response in
                    
                    print("Response:")
                    print(response.result)
                    
                    switch response.result {
                    case .success(let value):
                        
                        if let jsonData = value as? [[String: Any]] {
                            let SemesterData = self.getSemesterWith(jsonData)
                            completion(SemesterData)
                        } else {
                            completion([Semesters]())
                        }
                        
                    case .failure(let error):
                        print("Error:", error)
                        completion([Semesters]())
                    }
                    
                   }
    }
}

