//
//  StudentSemesterCollectionViewCell.swift
//  Fyp_Abdullah
//
//  Created by Shh MacBook Pro 2019 on 09/08/2021.
//

import UIKit

class StudentSemesterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var Semester:UILabel!
    @IBOutlet weak var SemesterNum:UILabel!
    @IBOutlet weak var SemesterStatus:UILabel!
    func SETUP(s:Semesters){
        Semester.text = s.Semester.description
        SemesterNum.text = s.SemesterNum.description
        SemesterStatus.text = s.Status.description
    }
}
