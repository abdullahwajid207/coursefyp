//
//  StudentFailedCoursesTableViewCell.swift
//  Fyp_Abdullah
//
//  Created by Shh MacBook Pro 2019 on 09/08/2021.
//

import UIKit

class StudentFailedCoursesTableViewCell: UITableViewCell {
    @IBOutlet weak var CourseName: UILabel!
    @IBOutlet weak var CourseCode: UILabel!
    @IBOutlet weak var CourseAttempt: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setData(s: Course1){
        CourseName.text = s.Course_no
        CourseCode.text = s.Grade
        CourseAttempt.text = s.ATTEMPT_NO
    }
}
