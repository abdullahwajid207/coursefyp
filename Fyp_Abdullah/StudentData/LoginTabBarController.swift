//
//  LoginTabBarController.swift
//  Fyp_Abdullah
//
//  Created by Abdullah on 11/08/2021.
//

import UIKit

class LoginTabBarController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()
        CourseAPIManager.StudentCourses(Reg_No: Appstatemanager.shared.loggedInUser.Reg_No ?? "", completion: {
            data in
                if ( data != nil){
                    CourseAPIManager.StudentFailedCourses(Reg_No: Appstatemanager.shared.loggedInUser.Reg_No ?? "", completion: {
                        fdata in
                            if ( fdata != nil){
                                
                                Appstatemanager.shared.loggedInUserFailedCourses = fdata
                                Appstatemanager.shared.loggedInUserCourses = data
                                StudentAPIManager.StudentSemester(completion: {sdata in
                                    if(sdata != nil){
                                        Appstatemanager.shared.Student = sdata
                                        SemesterAPIManager.StudentSemesterData(Reg_No: Appstatemanager.shared.loggedInUser.Reg_No ?? "", completion: {semdata in
                                            if(semdata != nil){
                                                Appstatemanager.shared.StudentSemester = semdata
                                            }
                                        })
                                    }
                                })
                            }
                        
                    })
                    
                }
            
        })
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
