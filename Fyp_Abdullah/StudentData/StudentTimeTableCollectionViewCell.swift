//
//  StudentTimeTableCollectionViewCell.swift
//  Fyp_Abdullah
//
//  Created by Shh MacBook Pro 2019 on 09/08/2021.
//

import UIKit

class StudentTimeTableCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var CourseName:UILabel!
    @IBOutlet weak var CourseTime:UILabel!
    func setup(s: TimeTable){
        CourseName.text = s.CourseName.description
        CourseTime.text = s.Time.description
    }
}
