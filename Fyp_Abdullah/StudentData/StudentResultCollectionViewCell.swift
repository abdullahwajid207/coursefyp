//
//  StudentResultCollectionViewCell.swift
//  Fyp_Abdullah
//
//  Created by Abdullah on 21/08/2021.
//

import UIKit

class StudentResultCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblCourse: UILabel!
    @IBOutlet weak var lblFinalScore: UILabel!
    @IBOutlet weak var lblGrade: UILabel!
    @IBOutlet weak var lblAttempt: UILabel!
    @IBOutlet weak var lblPreviousGrade: UILabel!
    func setup(data: Result){
        lblCourse.text = data.Course.description
        lblFinalScore.text = data.FinalScore.description
        lblGrade.text = data.Grade.description
        lblAttempt.text = data.Attempt.description
        if(data.PreviousGrade != nil){
        lblPreviousGrade.text = data.PreviousGrade.description
        }
        else{
            lblPreviousGrade.text = "None"
        }
    }
}
