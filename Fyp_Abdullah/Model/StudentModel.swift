//
//  StudentModel.swift
//  Fyp_Abdullah
//
//  Created by Abdullah on 11/08/2021.
//

import Foundation
class Student1 : Codable{
    var Reg_No: String?
    var App_no: String?
    var St_firstname: String?
    var St_lastname: String?
    var St_middlename: String?
    var Father_name: String?
    var St_email: String?
    var Sex: String?
    var Uaar_reg_no: String?
    var Marital_status: String?
    var Nic_no: String?
    var Pr_city: String?
    var Per_city: String?
    var Ad_status: String?
    var Sess: String?
    var Pref_1: String?
    var Pref_2: String?
    var Semester_no: String?
    var Remarks: String?
    var St_status: String?
    var USER_ID: String?
    var Acc_status: String?
    var St_assets: String?
    var Whom_STATUS: String?
    var Fin_status: String?
    var Final_course: String?
    var Enrl_status: String?
    var DOMICILE: String?
    var Degree_Received: String?
    var Deg_No: String?
    var Year: String?
    var SOS: String?
    var Section: String?
    var Password: String?
    var PMobile: String?
    static func save(_ data: Student1){
        
        UserDefaults.standard.set(codable: data, forKey: "loggedInUser")
        
    }
    
    
    static func delete(_ data: Student1){
        
        UserDefaults.standard.set(codable: Student1(), forKey: "loggedInUser")
        
    }
    
    
    static func get() -> Student1{
        
        return UserDefaults.standard.codable(Student1.self, forKey: "loggedInUser") ?? Student1()
    }
}
class Student{
    var Reg_NO:String!
    var Semester_No:String!
    var Semester_Status:String!
    var TotalCreditHoursCleared:String!
    var TotalCreditHours:String!
    var CGPA:String!
    var StudentName:String!
    var FatherName:String!
    var JoinYear:String!
    var Discipline:String!
    var Section:String!
}

class Courses{
    var RegNum:String!
    var CourseCode:String!
    var CourseStatus:String!
    var CourseName:String!
}
class TimeTable{
    var Section:String!
    var CourseName:String!
    var Time:String!
}

