//
//  SemesterModel.swift
//  Fyp_Abdullah
//
//  Created by Abdullah on 21/08/2021.
//

import Foundation
class Semesters{
    var Semester:String!
    var Status:String!
    var SemesterNum:Int!
    var CGPA:Double!
}
class Result{
    var Course:String!
    var FinalScore:Double!
    var Grade:String!
    var Attempt:String!
    var PreviousGrade:String!
}
