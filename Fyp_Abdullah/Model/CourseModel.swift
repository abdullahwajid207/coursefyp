//
//  CourseModel.swift
//  Fyp_Abdullah
//
//  Created by Abdullah on 16/08/2021.
//

import Foundation
class Course1 : Codable{
    var Course_no:String?
    var ATTEMPT_NO:String?
    var REG_NO:String?
    var Emp_no:String?
    var SEMESTER_NO:String?
    var Course_acw:String?
    var SECTION:String?
    var Final_score:String?
    var Midterm_score:String?
    var Assi_score:String?
    var Prac_score:String?
    var Grade:String?
    var Q_points:String?
    var USER_ID:String?
    var DISCIPLINE:String?
    var PRACA:String?
    var PRACB:String?
    var PRACC:String?
    var PRACD:String?
    var PRACE:String?
    var SOS:String?
    var pGrade:String?
    var Mid_att:String?
    var Fin_att:String?

}
