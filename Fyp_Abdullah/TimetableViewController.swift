//
//  TimetableViewController.swift
//  Fyp_Abdullah
//
//  Created by Apple on 08/08/2021.
//

import UIKit

class TimetableViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        tData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StudentTimeTableCollectionViewCell", for: indexPath as IndexPath) as! StudentTimeTableCollectionViewCell
        if(tData[indexPath.row].Time.prefix(3) == Day)
        {
        cell.setup(s: tData[indexPath.row])
        }
            return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 413.0, height: 60.0)
        }
    var tData = [TimeTable]()
    @IBOutlet weak var TimeCollectionView:UICollectionView!
    @IBOutlet weak var btnMonday:UIButton!
    @IBOutlet weak var btnTuesday:UIButton!
    @IBOutlet weak var btnWednesday:UIButton!
    @IBOutlet weak var btnThursday:UIButton!
    @IBOutlet weak var btnFriday:UIButton!
    var Day = String()
    @IBAction func btnMon(_ sender: Any){
        Day = "MON"
        TimeCollectionView.reloadData()
    }
    @IBAction func btnTuesday(_ sender: Any){
        Day = "TUE"
        TimeCollectionView.reloadData()
    }
    @IBAction func btnWednesday(_ sender: Any){
        Day = "WED"
        TimeCollectionView.reloadData()
    }
    @IBAction func btnThursday(_ sender: Any){
        Day = "THU"
        TimeCollectionView.reloadData()
    }
    @IBAction func btnFriday(_ sender: Any){
        Day = "FRI"
        TimeCollectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Day = "MON"
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        setupUI()
        setup(isDummy: true)
    }
    func setupUI(){
        TimeCollectionView.delegate = self
        TimeCollectionView.dataSource = self
    }
    func setup(isDummy: Bool){
        if(isDummy == true){
            var temp = TimeTable()
            temp.CourseName = "Financial Accounting"
            temp.Time = "8:30 - 10:00"
            tData.append(temp)
            temp = TimeTable()
            temp.CourseName = "Data Structures and Algorithms"
            temp.Time = "10:00 - 11:30"
            tData.append(temp)
        }
        else{
            //CALL API
        }
        TimeCollectionView.reloadData() //CALL THIS FUNCTION IN API COMPLETION HANDLER
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
