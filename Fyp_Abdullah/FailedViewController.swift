//
//  FailedViewController.swift
//  Fyp_Abdullah
//
//  Created by Apple on 08/08/2021.
//

import UIKit

class FailedViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        Appstatemanager.shared.loggedInUserFailedCourses?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StudentFailedCoursesTableViewCell", for: indexPath as IndexPath) as! StudentFailedCoursesTableViewCell
        
        cell.setData(s: Appstatemanager.shared.loggedInUserFailedCourses?[indexPath.row] ?? Course1())
        
        return cell
    }
    
    
    @IBOutlet weak var tableview: UITableView!
    var cData = [Courses]()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        
        setupData(isDummy: true)
        setupUI()
    }
    func setupUI(){
        tableview.delegate = self
        tableview.dataSource = self
    }
    func setupData(isDummy: Bool){
        if(isDummy == false){
            
        }
        else{
            //CALL API
        }
        tableview.reloadData() //CALL THIS FUNCTION IN COMPLETION HANDLER IN CASE OF API
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
