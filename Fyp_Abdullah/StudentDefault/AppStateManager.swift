//
//  AppStateManager.swift
//  Fyp_Abdullah
//
//  Created by Shh MacBook Pro 2019 on 16/08/2021.
//

import Foundation

class Appstatemanager : NSObject {
    
    static var shared = Appstatemanager()
    
    
    var loggedInUser: Student1! {
        get{
            Student1.get()
        } set {
            Student1.save(newValue)
        }
    }
    
    var isLoggedIn: Bool! {
        get { return UserDefaults.standard.bool(forKey: "isLoggedIn") }
        set { UserDefaults.standard.set(newValue, forKey: "isLoggedIn")
        }
    }
    var loggedInUserCourses: [Course1]?
    var loggedInUserFailedCourses: [Course1]?
    var Student: Student?
    var StudentSemester: [Semesters]?
}
