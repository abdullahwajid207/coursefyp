//
//  LoginViewController.swift
//  Fyp_Abdullah
//
//  Created by Apple on 08/08/2021.
//

import UIKit

class LoginViewController: UIViewController {
    var imageIcon = UIImageView()
    @IBOutlet weak var txtname: UITextField!
    @IBOutlet weak var txtpassword: UITextField!
   
    var iconClick = false
   
    override func viewDidLoad() {
        if(Appstatemanager.shared.isLoggedIn == true){
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginTabBarController") as! LoginTabBarController
            UIApplication.shared.windows.first?.rootViewController = viewController
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        }
        ImageToggle()
        txtpassword.isSecureTextEntry = true
   
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func login(_ sender: UIButton) {
        StudentAPIManager.StudentLogin(Reg_No: txtname.text?.description ?? "" , Password: txtpassword.text?.description ?? "", completion: {
            data in
            if(data != nil){
                                Appstatemanager.shared.loggedInUser = data
                                Appstatemanager.shared.isLoggedIn = true
                                
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                                let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginTabBarController") as! LoginTabBarController
                                UIApplication.shared.windows.first?.rootViewController = viewController
                                UIApplication.shared.windows.first?.makeKeyAndVisible()
                
            }
            else{
                self.displayErrorMessage(message: "Invalid UserName or Password")
            }
        })
    }
    
    
    

    
    func displayErrorMessage(message:String) {
        let alertView = UIAlertController(title: "Error!", message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertView.addAction(OKAction)
        if let presenter = alertView.popoverPresentationController {
            presenter.sourceView = self.view
            presenter.sourceRect = self.view.bounds
        }
        self.present(alertView, animated: true, completion:nil)
    }
    
    fileprivate func ImageToggle()  {
        
        /// --Toggle Image--
        
        imageIcon.image = UIImage(named: "eyeslash")
        let contentView = UIView()
        contentView.addSubview(imageIcon)
        contentView.translatesAutoresizingMaskIntoConstraints = false
        contentView.frame = CGRect(x: -10, y: 0, width: 20, height: 20)
        
        imageIcon.frame = CGRect(x: -10, y: 0, width: 20, height: 20)
        
        txtpassword.rightView = contentView
        txtpassword.rightViewMode = .always
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        imageIcon.isUserInteractionEnabled = true
        imageIcon.addGestureRecognizer(tapGestureRecognizer)
        
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        if iconClick{
            
            iconClick = false
            tappedImage.image = UIImage(named: "eye")
            txtpassword.isSecureTextEntry = false
        }
        else{
            iconClick = true
            tappedImage.image = UIImage(named: "eyelash")
            txtpassword.isSecureTextEntry = true
        }
        
    }
    
    func displayAlertMessage(userMessage: String, okTapped: @escaping() -> ()) {
        
        let myAlert = UIAlertController(title:"Alert", message: userMessage, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title:"Ok", style: UIAlertAction.Style.default) {
            action in
            self.dismiss(animated: true, completion:{
                okTapped()
            })
        }
        
        myAlert.addAction(okAction);
        
        self.present(myAlert, animated: true, completion: nil)
    }

}
