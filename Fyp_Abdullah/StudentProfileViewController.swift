//
//  StudentProfileViewController.swift
//  Fyp_Abdullah
//
//  Created by Apple on 08/08/2021.
//

import UIKit

class StudentProfileViewController: UIViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        (Appstatemanager.shared.StudentSemester?.count ?? 0) - 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StudentSemesterCollectionViewCell", for: indexPath as IndexPath) as! StudentSemesterCollectionViewCell
        cell.SETUP(s: Appstatemanager.shared.StudentSemester?[indexPath.row] ?? Semesters())
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 369.0, height: 42.0)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "StudentResultViewController") as! StudentResultViewController
        viewController.mainViewController = self
        viewController.SemesterNum = Appstatemanager.shared.StudentSemester?[indexPath.row].Semester ?? ""
        viewController.aCGPA = Appstatemanager.shared.StudentSemester?[indexPath.row].CGPA.description ?? ""
        viewController.aStatus = Appstatemanager.shared.StudentSemester?[indexPath.row].Status ?? ""
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    var SDATA = Student1()
    var SEMESTERDATA = [Semesters]()
    @IBOutlet weak var lblname: UILabel!
    @IBOutlet weak var lblFatherName: UILabel!
    @IBOutlet weak var lblJoinYear: UILabel!
    @IBOutlet weak var lblDiscipline: UILabel!
    @IBOutlet weak var lblSection: UILabel!
    @IBOutlet weak var lblEnrolledStatus: UILabel!
    @IBOutlet weak var lblTotalCreditHours: UILabel!
    
    
    @IBOutlet weak var lblregno: UILabel!
    @IBOutlet weak var SemCollectionView: UICollectionView!
    
    @IBOutlet weak var lblEnrolledSemester: UILabel!
    
    @IBOutlet weak var lblCurrentCGPA: UILabel!
    
    @IBOutlet weak var lblCreditHours: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setupData()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        setupUI()
        setup(isDummy: true)
    }
    func setupData(){
        lblname.text = Appstatemanager.shared.Student?.StudentName
        lblFatherName.text = "S/D of " +  (Appstatemanager.shared.Student?.FatherName ?? "")
        lblJoinYear.text = Appstatemanager.shared.Student?.JoinYear
        lblDiscipline.text = Appstatemanager.shared.Student?.Discipline
        lblSection.text = Appstatemanager.shared.Student?.Section
        lblEnrolledStatus.text = Appstatemanager.shared.Student?.Semester_Status
        lblEnrolledSemester.text = Appstatemanager.shared.Student?.Semester_No
        lblCurrentCGPA.text = Appstatemanager.shared.Student?.CGPA
        lblCreditHours.text = Appstatemanager.shared.Student?.TotalCreditHoursCleared
        lblTotalCreditHours.text = Appstatemanager.shared.Student?.TotalCreditHours
        lblregno.text = Appstatemanager.shared.loggedInUser.Reg_No?.description
        
    }
    func setupUI(){
        SemCollectionView.dataSource = self
        SemCollectionView.delegate = self
    }
    func setup(isDummy: Bool){
        if(isDummy == true){
            
        }
        else{
            //CALL API
        }
        SemCollectionView.reloadData()// IF CALLING API, THEN PUT THIS CALLING INSIDE COMPLETION HANDLER
    }
    @IBAction func btnLogout(_ sender: Any) {
        Appstatemanager.shared.isLoggedIn = false
        Appstatemanager.shared.loggedInUser = Student1()
        Appstatemanager.shared.loggedInUserCourses = [Course1]()
        Appstatemanager.shared.loggedInUserFailedCourses = [Course1]()
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        UIApplication.shared.windows.first?.rootViewController = viewController
        UIApplication.shared.windows.first?.makeKeyAndVisible()
        
    }
}
