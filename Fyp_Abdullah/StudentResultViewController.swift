//
//  StudentResultViewController.swift
//  Fyp_Abdullah
//
//  Created by Abdullah on 21/08/2021.
//

import UIKit

class StudentResultViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        resultData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StudentResultCollectionViewCell", for: indexPath as IndexPath) as! StudentResultCollectionViewCell
        cell.setup(data: resultData[indexPath.row])
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 413.0, height: 60.0)
        }
    var resultData = [Result]()
    var SemesterNum = String()
    var aCGPA = String()
    var aStatus = String()
    var mainViewController:StudentProfileViewController?
    @IBOutlet weak var SemesterNumber: UILabel!
    @IBOutlet weak var CGPA: UILabel!
    @IBOutlet weak var SemesterStatus: UILabel!
    @IBOutlet weak var ResultCollectionView: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ResultCollectionView.delegate = self
        ResultCollectionView.dataSource = self
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        setupData()
    }
    func setupData()
    {
        SemesterAPIManager.StudentSemesterResult(Sem_No: SemesterNum, completion: {data in
            if(data != nil){
                
                self.resultData = data ?? [Result]()
                self.ResultCollectionView.reloadData()
                self.SemesterNumber.text = self.SemesterNum.description
                self.CGPA.text = self.aCGPA.description
                self.SemesterStatus.text = self.aStatus.description
            }
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
